/**
 *  @filename   :   SmallFont.h
 *  @brief      :   It implements a char print for important ASCII chars with
 *                  19 lines a 30 characters on 152x152 eInk display.
 *  @author     :   Jochen Peters from Krefeld
 *
 *  Copyright (C) Jochen Peters, Krefeld in 30. December 2020
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
#define SMALLFONT_WIDTH 5

#include "epd1in54c.h"
#include <avr/pgmspace.h>

const uint8_t SmallFont_Tab1[] PROGMEM = 
{
  // 
  0b11111111,
  0b11111111,
  0b11111111,
  0b11111111,
  0b11111111,
  // !
  0b11111111,
  0b11111111,
  0b10100000,
  0b11111111,
  0b11111111,
  // "
  0b11111111,
  0b11111100,
  0b11111111,
  0b11111100,
  0b11111111,
  // #
  0b11101011,
  0b11000000,
  0b11101011,
  0b11000000,
  0b11101011,
  // $
  0b11111011,
  0b10110101,
  0b00000000,
  0b11001101,
  0b11111111,
  // %
  0b11111111,
  0b01110111,
  0b10011111,
  0b11100011,
  0b10111101,
  // &
  0b11010011,
  0b10100101,
  0b11011011,
  0b10101111,
  0b11111111,
  // '
  0b11111111,
  0b11111111,
  0b11111000,
  0b11111111,
  0b11111111,
  // (
  0b11111111,
  0b11111111,
  0b10000001,
  0b01111110,
  0b11111111,
  // )
  0b11111111,
  0b01111110,
  0b10000001,
  0b11111111,
  0b11111111,
  // *
  0b11111111,
  0b11110101,
  0b11111000,
  0b11110101,
  0b11111111,
  // +
  0b11101111,
  0b11101111,
  0b10000011,
  0b11101111,
  0b11101111,
  // ,
  0b11111111,
  0b00111111,
  0b11011111,
  0b11111111,
  0b11111111,
  // -
  0b11111111,
  0b11101111,
  0b11101111,
  0b11101111,
  0b11111111,
  // .
  0b11111111,
  0b11111111,
  0b10011111,
  0b10011111,
  0b11111111,
  // /
  0b11111111,
  0b10111111,
  0b11001111,
  0b11110001,
  0b11111110,
  // 0
  0b11111111,
  0b11000011,
  0b10111101,
  0b11000011,
  0b11111111,
  // 1
  0b11111111,
  0b11011101,
  0b11000000,
  0b11011111,
  0b11111111,
  // 2
  0b11111111,
  0b10011101,
  0b10100101,
  0b10111011,
  0b11111111,
  // 3
  0b11111111,
  0b10111011,
  0b10101101,
  0b11010011,
  0b11111111,
  // 4
  0b11100011,
  0b11101101,
  0b10000000,
  0b11101111,
  0b11111111,
  // 5
  0b11111111,
  0b11111000,
  0b10110110,
  0b11001110,
  0b11111111,
  // 6
  0b11111111,
  0b11000111,
  0b10101011,
  0b11011101,
  0b11111111,
  // 7
  0b11111111,
  0b11101110,
  0b10000110,
  0b11101000,
  0b11111111,
  // 8
  0b11111111,
  0b11001001,
  0b10110110,
  0b11001001,
  0b11111111,
  // 9
  0b11111111,
  0b11111001,
  0b10110110,
  0b11000001,
  0b11111111,
  // :
  0b11111111,
  0b10011001,
  0b10011001,
  0b11111111,
  0b11111111,
  // ;
  0b11111111,
  0b00111111,
  0b11010111,
  0b11111111,
  0b11111111,
  // <
  0b11110111,
  0b11101011,
  0b11101011,
  0b11011101,
  0b11111111,
  // =
  0b11111111,
  0b11010111,
  0b11010111,
  0b11010111,
  0b11111111,
  // >
  0b11011101,
  0b11101011,
  0b11101011,
  0b11110111,
  0b11111111,
  // ?
  0b11111011,
  0b11111101,
  0b00101101,
  0b00110011,
  0b11111111,
  // @
  0b11000001,
  0b10111110,
  0b10100010,
  0b11110001,
  0b11111111
};

const uint8_t SmallFont_Tab2[] PROGMEM = 
{
  // A
  0b11111111,
  0b10000011,
  0b11101101,
  0b10000011,
  0b11111111,
  // B
  0b11111111,
  0b10000000,
  0b10110110,
  0b11001001,
  0b11111111,
  // C
  0b11111111,
  0b11000001,
  0b10111110,
  0b11011101,
  0b11111111,
  // D
  0b11111111,
  0b10000000,
  0b10111110,
  0b11000001,
  0b11111111,
  // E
  0b11111111,
  0b10000000,
  0b10110110,
  0b10111110,
  0b11111111,
  // F
  0b11111111,
  0b10000000,
  0b11110110,
  0b11110110,
  0b11111111,
  // G
  0b11000001,
  0b10111110,
  0b10101110,
  0b11001101,
  0b11111111,
  // H
  0b10000000,
  0b11110111,
  0b11110111,
  0b10000000,
  0b11111111,
  // I
  0b11111111,
  0b10111110,
  0b10000000,
  0b10111110,
  0b11111111,
  // J
  0b11111111,
  0b10111111,
  0b01111110,
  0b10000000,
  0b11111111,
  // K
  0b11111111,
  0b10000000,
  0b11101011,
  0b10011100,
  0b11111111,
  // L
  0b11111111,
  0b10000000,
  0b10111111,
  0b10111111,
  0b11111111,
  // M
  0b10000000,
  0b11111011,
  0b11110111,
  0b11111011,
  0b10000000,
  // N
  0b10000001,
  0b11111011,
  0b11100111,
  0b10000001,
  0b11111111,
  // O
  0b10000001,
  0b01111110,
  0b01111110,
  0b10000001,
  0b11111111,
  // P
  0b11111111,
  0b10000000,
  0b11110110,
  0b11111001,
  0b11111111,
  // Q
  0b11000011,
  0b10111101,
  0b10011101,
  0b10000011,
  0b01111111,
  // R
  0b11111111,
  0b10000000,
  0b11100110,
  0b10011001,
  0b11111111,
  // S
  0b11111111,
  0b10111001,
  0b10110110,
  0b11001110,
  0b11111111,
  // T
  0b11111111,
  0b11111110,
  0b10000000,
  0b11111110,
  0b11111111,
  // U
  0b11000001,
  0b10111111,
  0b10111111,
  0b11000001,
  0b11111111,
  // V
  0b11111111,
  0b11100000,
  0b10011111,
  0b11100000,
  0b11111111,
  // W
  0b11000001,
  0b10111111,
  0b11001111,
  0b10111111,
  0b11000001,
  // X
  0b11111111,
  0b10011100,
  0b11100011,
  0b10011100,
  0b11111111,
  // Y
  0b11111111,
  0b11110001,
  0b10001111,
  0b11110001,
  0b11111111,
  // Z
  0b11111111,
  0b10011110,
  0b10100110,
  0b10111000,
  0b11111111,
  // [
  0b11111111,
  0b11111111,
  0b00000000,
  0b01111110,
  0b11111111,
  // \
  0b11111110,
  0b11111001,
  0b11000111,
  0b00111111,
  0b11111111,
  // ]
  0b11111111,
  0b01111110,
  0b00000000,
  0b11111111,
  0b11111111,
  // ^
  0b11111111,
  0b11111101,
  0b11111110,
  0b11111101,
  0b11111111,
  // _
  0b10111111,
  0b10111111,
  0b10111111,
  0b10111111,
  0b10111111,
  // `
  0b11111111,
  0b11111111,
  0b11111110,
  0b11111101,
  0b11111111
};

const uint8_t SmallFont_Tab3[] PROGMEM = 
{
  // a
  0b11111111,
  0b11001111,
  0b10110111,
  0b10000111,
  0b11111111,  
  // b
  0b11111111,
  0b11000000,
  0b10110111,
  0b10001111,
  0b11111111,
  // c
  0b11111111,
  0b11001111,
  0b10110111,
  0b10110111,
  0b11111111,
  // d
  0b11111111,
  0b11001111,
  0b10110111,
  0b10000000,
  0b11111111,
  // e
  0b11111111,
  0b11001111,
  0b10100111,
  0b10100111,
  0b11111111,
  // f
  0b11111111,
  0b11111011,
  0b10000001,
  0b11111010,
  0b11111111,
  // g
  0b11111111,
  0b01001111,
  0b01010111,
  0b10000111,
  0b11111111,
  // h
  0b11111111,
  0b10000000,
  0b11101111,
  0b10011111,
  0b11111111,
  // i
  0b11111111,
  0b10110111,
  0b10000101,
  0b10111111,
  0b11111111,
  // j
  0b11111111,
  0b01101111,
  0b10001011,
  0b11111111,
  0b11111111,
  // k
  0b11111111,
  0b10000000,
  0b11011111,
  0b10101111,
  0b11111111,
  // l
  0b11111111,
  0b11000001,
  0b10111111,
  0b11111111,
  0b11111111,
  // m
  0b10001111,
  0b11101111,
  0b10001111,
  0b11101111,
  0b10011111,
  // n
  0b11111111,
  0b10001111,
  0b11101111,
  0b10011111,
  0b11111111,
  // o
  0b11111111,
  0b11001111,
  0b10110111,
  0b11001111,
  0b11111111,
  // p
  0b11111111,
  0b00001111,
  0b11010111,
  0b11001111,
  0b11111111,
  // q
  0b11111111,
  0b11001111,
  0b11010111,
  0b00001111,
  0b11111111,
  // r
  0b11111111,
  0b10001111,
  0b11110111,
  0b11111111,
  0b11111111,
  // s
  0b11111111,
  0b01101111,
  0b01010111,
  0b10110111,
  0b11111111,
  // t
  0b11111111,
  0b11111011,
  0b11000000,
  0b10111011,
  0b11111111,
  // u
  0b11111111,
  0b10001111,
  0b10111111,
  0b10001111,
  0b11111111,
  // v
  0b11111111,
  0b11001111,
  0b10111111,
  0b11001111,
  0b11111111,
  // w
  0b11001111,
  0b10111111,
  0b11011111,
  0b10111111,
  0b11001111,
  // x
  0b11111111,
  0b10101111,
  0b11011111,
  0b10101111,
  0b11111111,
  // y
  0b11111111,
  0b01110111,
  0b10101111,
  0b11000111,
  0b11111111,
  // z
  0b11111111,
  0b10010111,
  0b10100111,
  0b10110111,
  0b11111111,
  // {
  0b11111111,
  0b11101111,
  0b10010001,
  0b01111110,
  0b11111111,
  // |
  0b11111111,
  0b11111111,
  0b00000000,
  0b11111111,
  0b11111111,
  // }
  0b11111111,
  0b01111110,
  0b10010001,
  0b11101111,
  0b11111111,
  // ~
  0b11111011,
  0b11110111,
  0b11111011,
  0b11110111,
  0b11111111
};


void SmallFont_print(Epd &eink, char *text, int tsize, bool isRed, bool inverse) {
  unsigned int charact;
  const unsigned char* ptr;
  if (isRed) {
    eink.SendCommand(DATA_START_TRANSMISSION_2); // Red (or Yellow)    
  } else {
    eink.SendCommand(DATA_START_TRANSMISSION_1); // Black
  }
  
  delay(2);
  for (int row = 0; row < eink.height; row++) {
    for (int column = 0; column < (eink.width / 8); column++) {
      
      if (row < ((tsize-1)*SMALLFONT_WIDTH) && column >= ((eink.width / 8)-1 - ((tsize-2)/(eink.height/SMALLFONT_WIDTH)) )) {

        if ((row/SMALLFONT_WIDTH) + (eink.height/SMALLFONT_WIDTH)*((eink.width / 8)-1 - column) < (tsize-1)) {
          charact = text[(row/SMALLFONT_WIDTH) + (eink.height/SMALLFONT_WIDTH)*((eink.width / 8)-1 - column)];
          if (charact >= 'a') {
            ptr = &SmallFont_Tab3[(charact - 'a') * SMALLFONT_WIDTH];
          } else if (charact >= 'A') {
            ptr = &SmallFont_Tab2[(charact - 'A') * SMALLFONT_WIDTH];
          } else {
            ptr = &SmallFont_Tab1[(charact - ' ') * SMALLFONT_WIDTH];
          }

          if ((inverse == true) && (charact != ' ')) {
            eink.SendData(~pgm_read_byte(ptr+(row%SMALLFONT_WIDTH)));
          } else {
            eink.SendData(pgm_read_byte(ptr+(row%SMALLFONT_WIDTH)));
          }

        } else {
          eink.SendData(0b11111111);
        }
          
      } else {
        eink.SendData(0b11111111);
      }
    }
  }
  delay(2);
}

