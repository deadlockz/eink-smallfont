#include <SPI.h>
#include "epd1in54c.h"
#include "SmallFont.h"

Epd epd;

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    if (epd.Init() != 0) {
        Serial.print("e-Paper init failed");
        return;
    }
    epd.Clear();

    char text[]  =  "Hallo Welt, und hallo  a d  !!"
                    " ;-)   Toll? >-D {        }   "
                    "---===++ Welt: Toll.          "
                    "                              "
                    " The MIT License              "
                    "Permission is hereby granted, "
                    "free of charge, to any per-   "
                    "son obtaining a copy of this  "
                    "software and associated docum-"
                    "entation ...";
    char text2[] =  "                      S_n_ra  "
                    "                  Klammern";

    SmallFont_print(epd, text,  sizeof(text), false, false); // not 2nd color, not inverse
    SmallFont_print(epd, text2, sizeof(text2), true, false); // 2nd color, not inverse
    epd.SendCommand(DISPLAY_REFRESH);
    epd.WaitUntilIdle();
    epd.Sleep();
}

void loop() {

}
