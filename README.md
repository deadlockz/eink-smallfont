# Small print for e-Paper Display

It uses a library to control waveshare 1.54 inch e-paper display. I write
additional code to implement a "Print" for Arduino with a very small font.

- Target [152x152, 1.54inch E-Ink display module, yellow/black/white three-color](https://www.waveshare.com/product/modules/oleds-lcds/e-paper/1.54inch-e-paper-module-c.htm)
- 5x8 font
- uses the 19 byte lines of the display to print 19 lines with 30 characters each
- inverse and the 2nd color is possible

Code is still a bit buggy or maybe the exact pixel set of the display
is buggy.

## Files

- `SmallFont.h` is my font and print code
- `epd*` are the original waveshare lib files
- `code_EPD1in54C.ino` is a example (tested on Arduino IDE 1.8.1)

## Images

![example code](example.jpg)

![example code in reality](result-example-code.jpg)